Программирование *
программирование, c++, java, python, javascript, .net, c#, php, разработка, c

Разработка игр *
игры, game development, gamedev, unity3d, разработка игр, android, ios, unity, c++, разработка

Разработка веб-сайтов *
javascript, web-разработка, css, php, html, html5, jquery, новости, веб-дизайн, ссылки

Информационная безопасность *
информационная безопасность, безопасность, взлом, security, уязвимость, уязвимости, google, вирусы, android, криптография

JavaScript *
javascript, jquery, node.js, html5, html, css, web-разработка, angularjs, canvas, браузеры

Системное администрирование *
linux, системное администрирование, windows, мониторинг, active directory, zabbix, nginx, виртуализация, powershell, администрирование

Машинное обучение *
машинное обучение, machine learning, big data, data mining, нейронные сети, python, большие данные, data science, анализ данных, kaggle

Алгоритмы *
алгоритмы, алгоритм, машинное обучение, программирование, математика, структуры данных, java, python, оптимизация, c++

Математика *
математика, алгоритмы, геометрия, wolfram mathematica, wolfram language, статистика, программирование, теория вероятностей, яндекс, машинное обучение

C++ *
c++, с++, c++11, c, qt, pvs-studio, программирование, си++, статический анализ кода, c plus plus

Python *
python, django, python3, flask, linux, tornado, питон, web-разработка, программирование, pycharm

Open source *
open source, linux, openstack, reactos, mirantis, microsoft, android, javascript, google, java

IT-инфраструктура *
цод, инфраструктура, дата-центр, хостинг, ит-инфраструктура, дата-центры, схд, hp, серверы, сервер

Управление разработкой *
разработка, тестирование, .net, программирование, visual studio, connect, microsoft, оптимизация, jenkins, agile

Высокая производительность *
highload, производительность, ibm, высокая производительность, php, dell, big data, оптимизация, java, opencl

Обработка изображений *
обработка изображений, opencv, компьютерное зрение, фотография, распознавание образов, computer vision, python, ocr, c++, алгоритмы

Ненормальное программирование *
ненормальное программирование, программирование, javascript, python, c++, brainfuck, c, java, .net, bash

Настройка Linux *
linux, ubuntu, debian, bash, open source, windows, gnome, unix, python, gentoo

Криптография *
криптография, bitcoin, шифрование, ssl, информационная безопасность, openssl, tls, биткоин, криптовалюта, rsa

Серверное администрирование *
linux, hp, мониторинг, схд, centos, nginx, виртуализация, fujitsu, системное администрирование, цод

Анализ и проектирование систем *
проектирование, аналитика, архитектура, разработка, анализ, автоматизация, бизнес-анализ, управление проектами, программирование, моделирование предметной области

Data Mining *
data mining, machine learning, big data, анализ данных, data science, машинное обучение, data science digest, python, r, high scalability

Java *
java, android, jvm, maven, spring, программирование, intellij idea, hibernate, oracle, groovy

Разработка под Android *
android, android development, ios, разработка приложений, java, монетизация, windows phone, маркетинг, мобильные устройства, мобильные приложения

.NET *
.net, c#, asp.net, c, wpf, asp.net mvc, visual studio, microsoft, silverlight, javascript

Компиляторы *
компиляторы, c++, llvm, gcc, clang, jit, компилятор, .net, java, c

Node.JS *
node.js, javascript, nodejs, npm, node, web-разработка, io.js, node-webkit, mongodb, framework

C# *
c#, .net, wpf, c#.net, visual studio, программирование, microsoft, unity3d, asp.net, silverlight

*nix *
linux, freebsd, unix, bash, debian, ubuntu, open source, python, linux kernel, centos

Big Data *
big data, data mining, machine learning, data science, hadoop, анализ данных, большие данные, машинное обучение, data science digest, аналитика

Разработка мобильных приложений *
android, ios, мобильные приложения, windows phone, mobile development, разработка приложений, монетизация, маркетинг, мобильная разработка, мобильные устройства

Реверс-инжиниринг *
reverse engineering, assembler, reactos, отладка, исследование программ, реверс-инжиниринг, ollydbg, игры, android, реверсинг

C *
c, c++, программирование, linux, pvs-studio, си, статический анализ кода, си++, open source, микроконтроллеры

PHP *
php, web-разработка, yii, laravel, framework, дайджест, ссылки, новости, подборка, mysql

Отладка *
отладка, reverse engineering, assembler, исследование программ, ollydbg, миландр, arm, к1986ве92qi, микроконтроллеры, cortex-m3

Визуализация данных *
визуализация данных, визуализация, r, wolfram language, wolfram mathematica, анализ данных, аналитика, инфографика, visualization, dataviz

Сетевые технологии *
cisco, wi-fi, сетевые технологии, vpn, mikrotik, сети, linux, juniper, ipv6, интернет

CSS *
css, html, css3, javascript, вёрстка, web-разработка, html5, веб-дизайн, браузеры, jquery

VIM *
vim, ide, python, linux, emacs, javascript, windows, редакторы, plugin, vimscript

HTML *
html, css, html5, javascript, canvas, web-разработка, css3, вёрстка, jquery, интересности&полезности

PostgreSQL *
postgresql, postgres, базы данных, sql, субд, постгрес, mysql, pgsql, highload, oracle

Хранение данных *
схд, хранение данных, netapp, netapp fas, цод, ssd, хостинг, emc, san, дата-центр

Программирование микроконтроллеров *
микроконтроллеры, stm32, avr, arm, программирование, arduino, freertos, pic, msp430, программирование микроконтроллеров

Angular *
angularjs, javascript, angular, angular.js, web-разработка, javascript framework, react.js, node.js, директивы, cordova

SQL *
sql, базы данных, sql server, mysql, oracle, postgresql, mssql, субд, t-sql, olap

Интерфейсы *
юзабилити, интерфейсы, интерфейс, ui, дизайн, ux, проектирование интерфейсов, пользовательские интерфейсы, user experience, дизайн интерфейсов

API *
api, javascript, .net, php, rest, nanocad, яндекс, программирование, oauth, multicad

IT-стандарты *
стандарты, html5, w3c, веб-стандарты, spdy, html, cda, hl7, google, ccd

Разработка под Windows *
windows, windows 10, microsoft, .net, uwp, c#, c++, wpf, windows phone, pvs-studio

Хабрахабр API *
хабрахабр, google chrome, extensions, habrajax, расширения, habrastorage, опера, opera, производительность, хром

Виртуализация *
виртуализация, vmware, linux, hyper-v, kvm, virtualization, veeam, xen, резервное копирование, виртуальная машина

Разработка под iOS *
ios, objective-c, android, ios development, iphone, apple, разработка приложений, маркетинг, windows phone, app store

Спортивное программирование *
спортивное программирование, олимпиадное программирование, mail.ru, конкурс, программирование, rcc, соревнование, acm icpc, олимпиада, imagine cup

ReactJS *
javascript, react.js, react, reactjs, flux, node.js, facebook, web-разработка, summaryjs, angular.js

Go *
go, golang, языки программирования, javascript, программирование, benchmark, ооп, docker, html, rust

Занимательные задачки *
математика, wolfram language, задача, wolfram mathematica, логика, задачки, алгоритмы, занимательные задачки, занимательные задачи, головоломки

Поисковые технологии *
google, поиск, яндекс, поисковые системы, nigma, нигма, seo, поисковые технологии, вебальта, yahoo

Тестирование IT-систем *
тестирование, автоматизация тестирования, qa, тестирование по, selenium, tdd, testing, java, разработка, нагрузочное тестирование

Промышленное программирование *
автоматизация, плк, scada, микроконтроллеры, асу тп, программирование, wms, plc, промышленная автоматизация, erp

Антивирусная защита *
вирусы, malware, антивирусы, информационная безопасность, антивирус, лаборатория касперского, klsw, касперский, kaspersky, безопасность

Системное программирование *
linux, uefi, kernel, windows, системное программирование, linux kernel, c, c++, assembler, программирование

Веб-дизайн *
веб-дизайн, css, web-разработка, javascript, html, ссылки, новости, css3, браузеры, дайджест

Открытые данные *
открытые данные, opendata, открытое государство, api, электронное государство, european social survey, рои, 3d-принтер, diy или сделай сам, мастер кит

Usability *
юзабилити, интерфейсы, ux, дизайн, ui, проектирование интерфейсов, пользовательские интерфейсы, веб-дизайн, android, юзабилити интерфейсов

Администрирование баз данных *
базы данных, postgresql, mysql, sql, oracle, sql server, субд, intersystems cache, nosql, intersystems

Параллельное программирование *
параллельное программирование, java, c++, openmp, c++11, multithreading, многопоточность, lock-free, cuda, многопоточное программирование

Функциональное программирование *
функциональное программирование, haskell, scala, functional programming, clojure, java, javascript, f#, erlang, монады

Разработка робототехники *
arduino

R *
r, анализ данных, статистика, data mining, машинное обучение, network, shiny, kaggle, программирование, european social survey

Git *
git, github, dvcs, svn, разработка, системы управления версиями, vcs, gitlab, git workflow, ssh

Совершенный код *
совершенный код, программирование, c++, сортировка расчёской, рефакторинг, best practices, python, design patterns, code review, java

Работа с 3D-графикой *
3d, анимация, blender, unity3d, 3d-графика, pixar, opengl, графика, 3d графика, unity

Kotlin *
kotlin, jetbrains, java, android, android development, ios, конференция, mbltdev, #mblt, #mbltdev

Дизайн мобильных приложений *
дизайн, android, мобильные приложения, дизайн интерфейсов, material design, пользовательские интерфейсы, конкурс, продуктовый дизайн, приложения, android development

Разработка под e-commerce *
e-commerce, электронная коммерция, ecommerce, интернет-магазин, интернет-магазины, astound commerce, стартапы, magento, конверсия, email-маркетинг

Тестирование веб-сервисов *
тестирование, qa, host-tracker, web-разработка, мониторинг сайта, мониторинг сервера, мониторинг, автоматизация тестирования, monitoring, веб-сервисы

Unreal Engine *
unreal engine 4, unreal engine, game development, игры, c++, новости, дайджест, разработка, си++, pvs-studio

NoSQL *
nosql, mongodb, redis, базы данных, cassandra, intersystems cache, sql, tarantool, mysql, cache

Service Desk *
service desk, jira, helpdesk, confluence, mail.ru, help desk, atlassian, служба поддержки, техподдержка, тикет-система

Графические оболочки *
kde, kde4, linux, amarok, gnome, сапр, nanocad, de, gentoo, wm

Firefox
firefox, mozilla, браузеры, javascript, mozilla firefox, firefox 4, firefox os, google chrome, firefox 3, расширения

Работа со звуком *
музыка, звук, звук и музыка, аудио, dsp, vst, работа со звуком, au, перевод, распознавание речи

Геоинформационные сервисы *
карты, 2гис, google maps, геолокация, gps, яндекс.карты, яндекс, android, google street view, google

Unity3D *
unity3d, unity, game development, gamedev, c#, разработка игр, игры, android, unity3d уроки, ios

Yii *
yii, php, framework, yii framework, yii2, yii 2, yiiframework, active record, jii, крутотень

Облачные вычисления *
облачные вычисления, windows azure, облачные сервисы, cloud, cloud computing, облака, облачные технологии, azure, iaas, microsoft

Типографика *
типографика, шрифты, шрифт, свободные шрифты, бесплатный шрифт, бесплатные шрифты, веб-дизайн, font-face, дизайн, типограф

Google Chrome
google chrome, google, javascript, chromium, extensions, браузеры, расширения, chrome os, firefox, extension

Проектирование и рефакторинг *
проектирование, рефакторинг, ооп, .net, архитектура приложений, программирование, архитектура, паттерны проектирования, php, разработка

Assembler *
assembler, reverse engineering, ассемблер, отладка, ollydbg, kolibrios, исследование программ, kolibri, игры, колибри

Разработка систем связи *
asterisk, voip, ip-телефония, sip, телефония, skype, сотовая связь, freeswitch, webrtc, digium

WebGL *
webgl, javascript, three.js, html5, blend4web, blender, разработка игр, 3d, web-разработка, космос

Хабрахабр — Анонсы
хабрахабр, тостер, хабрановшество, блогосфера, фрилансим, фриланс, теги, обновление, карма, toster

GitHub
github, git, гитхаб, open source, редактор кода, программирование, .net, студенты, запрос на слияние, github api

Расширения для браузеров
google chrome, расширения, браузеры, javascript, chrome extension, firefox, extensions, браузерные расширения, windows, gecko

Стандарты связи
lte, yota, 4g, wimax, 3g, мегафон, wi-fi, gsm, радиосвязь, йота

Scala *
scala, java, functional programming, play framework, akka, функциональное программирование, dsl, clojure, scaladev, программирование

Help Desk Software *
helpdesk, service desk, support, служба поддержки, otrs, itsm, open source, itil, itsm365, техподдержка

Microsoft SQL Server *
sql server, sql, t-sql, microsoft sql server, mssql, ms sql server, базы данных, microsoft, sql server 2012, ms sql

Системы управления версиями *
git, svn, mercurial, subversion, dvcs, github, vcs, hg, системы контроля версий, системы управления версиями

Беспроводные технологии *
wi-fi, беспроводные сети, wlan, lte, ubiquiti, 4g, nfc, bluetooth, gsm, беспроводная связь

Яндекс API *
яндекс, api, api яндекс, яндекс.диск, javascript, yandex api, яндекс.карты, yandex map api, яндекс.метрика, webdav

Профессиональная литература
книги, батлер, поулсен, издательство питер, кардинг, javascript, ux, пол грэм, фондовый рынок, стартапы

Хостинг
хостинг, цод, vps, дата-центр, домены, hosting, infoboxcloud, облачный хостинг, vds, infobox

Серверная оптимизация *
nginx, оптимизация, fujitsu, сервер, hp, php, сервера, производительность, centos, highload

Разработка систем передачи данных *
cda, ccd, hl7, gsm, fhir, метрология, osmocombb, тестирование сетей передачи данных, сети передачи данных, измерения

Хакатоны
хакатон, hackathon, hackday, хакатоны, хакдэй, стартапы, казань, набережные челны, harvest, microsoft

Платежные системы *
bitcoin, платёжные системы, paypal, яндекс.деньги, webmoney, qiwi, криптовалюта, mastercard, биткоин, web-payment.ru

Haskell *
haskell, функциональное программирование, хаскель, монады, теория категорий, ленивые вычисления, функторы, tutorial, c++, quest

Сжатие данных *
сжатие данных, сжатие, gzip, bzip2, rar, zip, архиваторы, теория информации, winrar, группа компаний мук

Swift *
swift, ios, ios development, objective-c, ios разработка, apple, xcode, mbltdev, e-legion, digest

Accessibility *
accessibility, доступность, вспомогательные технологии, android, юзабилити, доступность контента, яндекс, слепые люди, wai-aria, wcag

Системы обмена сообщениями *
im, jabber, icq, xmpp, miranda, мессенджеры, aol, qip, android, skype

SaaS / S+S *
saas, saas сервисы, ip-телефония, fmc, виртуальная атс, voip, crm, стартапы, ip-pbx, ip-атс

Perl *
perl, perl6, cpan, perl 6, mojolicious, rakudo, yapp, программирование, yapc, fastcgi

Работа с видео *
видео, youtube, видеонаблюдение, ffmpeg, h.264, видеоаналитика, google, linux, кино, компьютерное зрение

Локализация продуктов *
локализация, alconost, перевод, localization, мобильные приложения, app store, интернационализация, китай, rtl, ключевые слова

XML *
xml, xslt, xpath, dita, python, blogger, java, шаблоны, blogspot, php

Nginx *
nginx, lua, кеширование, linux, apache, debian, php, ddos, https, nginx patch

Терминология IT *
терминология, онтология, моделирование предметной области, перевод, парадигмы, бизнес-анализ, ооп, bpmn, русский язык, логика

Rust *
rust, программирование, c++, релиз, языки программирования, go, системное программирование, c, 3d-графика, rest

Oracle *
oracle, oracle database, sql, java, базы данных, субд, pl/sql, postgresql, .net, cluster

Клиентская оптимизация *
оптимизация, производительность, javascript, скорость загрузки, css, клиентская оптимизация, css sprites, web-разработка, internet explorer, gzip

FPGA *
fpga, xilinx, altera, timing constraints, static timing analysis, verilog, плис, vhdl

CRM-системы *
crm, crm-системы, crm система, erp, saas сервисы, saas, fmc, asterisk, бизнес-процессы, ip-телефония

ERP-системы *
erp, erp-системы, sap, crm, erp системы, автоматизация бизнеса, abap, 1с, ecm, crm система

Тестирование мобильных приложений *
тестирование, android, qa, ios, разработка, мобильные приложения, автоматизация тестирования, mobile development, дистрибуция, тестирование по

Восстановление данных *
резервное копирование, восстановление данных, backup, veeam, бэкап, виртуализация, acronis, veeam backup and replication, виртуальная машина, veeam backup

Erlang/OTP *
erlang, функциональное программирование, otp introduction, rabbitmq, elixir, для самых маленьких, otp, процессы, php, никакой императивной скверны

Cisco *
cisco, asa, routing, сетевые технологии, ccnp, ospf, ccna, ios, маршрутизатор, ipv6

Спам и антиспам
спам, антиспам, рассылки, капча, email, captcha, фишинг, социальные сети, ботнет, mail.ru

Ajax *
ajax, javascript, jquery, web-разработка, prototype, xmlhttprequest, web 2.0, framework, php, web

Децентрализованные сети
p2p, bittorrent, torrent, the pirate bay, bitcoin, utorrent, торренты, peer-to-peer, торрент, копирайт

Visual Studio *
visual studio, .net, visual studio 2010, microsoft, c#, visual studio 2012, c++, visual studio 2013, visual studio 2015, resharper

Резервное копирование *
резервное копирование, backup, veeam, виртуализация, veeam backup and replication, бэкап, виртуальная машина, acronis, восстановление данных, veeam backup

Разработка под OS X *
ios, objective-c, mac os x, os x, osx, , xcode, apple, ios development, macos

Django *
django, python, django framework, orm, nginx, web-разработка, pycharm, virtualenv, python3, postgresql

MongoDB *
mongodb, nosql, python, базы данных, php, node.js, mysql, javascript, sql, mongo

SCADA *
scada, умный дом, асутп, arduino, жкх, плк, асу тп, plc, open source, промышленная автоматизация

Qt *
qt, qml, c++, qt quick, qt5, nokia, qt4, qt creator, qt software, qt 5

Семантика *
semantic web, rdf, онтология, моделирование предметной области, owl, sparql, web 3.0, микроформаты, семантическая сеть, семантическая паутина

GPGPU *
cuda, gpgpu, nvidia, gpu, opencl, параллельное программирование, udacity, оптимизация, amd app, rendering

Sphinx *
sphinx, поиск, sphinx search, search, sphinxql, sphinxsearch, php, conference, полнотекстовый поиск, fulltext search

Разработка для Office 365 *
#officeplatform, office 365, office, office store, кроссплатформенность, html5, web-разработка, open source, javascript, мобильный веб

CMS *
cms, php, drupal, web-разработка, wordpress, joomla, open source, web 2.0, drupal-digest, modx

Ruby on Rails *
ruby on rails, ruby, конференция, railsclub, ruby on rails 3, конференция веб-разработчиков, javascript, activerecord, open source, railsclub.ru

ООП *
ооп, php, c++, javascript, c#, smalltalk, паттерны проектирования, mvc, паттерны, логическая парадигма

Delphi *
delphi, embarcadero, firemonkey, rad studio, android, vcl, delphi 2010, c++ builder, программирование, freepascal

ECM/СЭД *
ecm, сэд, электронный документооборот, документооборот, электронный архив, sharepoint, эос, erm, управление записями, сэа

Верстка писем *
верстка писем, email, html, почтовые рассылки, css, email-маркетинг, электронная почта, дизайн, верстка email, рассылки

Twitter API *
twitter, twitter api, python, big data, социальные сети, r

Brainfuck *
brainfuck, python, квайн, assembler, брейнфак

Кодобред
php, кодобред, программирование, javascript, бред, wtf_code, код, java, wtf, python

Objective C *
Компилируемый объектно-ориентированный язык
objective-c, ios, ios development, ios разработка, xcode, swift, iphone, разработка под ios, cocoa, core data

Google API *
google, google api, android, google apps script, google docs, google analytics, java, php, api, javascript

Ruby *
ruby, ruby on rails, конференция, программирование, railsclub, gem, sinatra, конференция веб-разработчиков, javascript, rubygems

Microsoft Azure
azure, windows azure, microsoft azure, microsoft, cloud, visual studio, .net, облако, облачные сервисы, iaas

Тестирование игр *
игры, разработка, game development, дайджест, разработка игр, новости, игровая индустрия, арт, графика, геймдизайн

IIS *
iis, asp.net, c#

Amazon Web Services *
aws, amazon, ec2, amazon web services, amazon s3, s3, cloud, rds, ebs, amazon ec2

Asterisk *
asterisk, voip, ip-телефония, freepbx, 3cx, телефония, sip, digium, saas сервисы, crm

Safari
safari, apple, firefox, браузеры, safari 5, расширение, mac, beta, webkit, хабрахабр

DNS *
dns, icann, bind, dynamic dns, ddos, tld, dyndns, домены, gtld, rpz

Администрирование доменных имен *
домены, icann, домен, киберсквоттинг, доменные имена, google, godaddy, ru-center, доменные споры, nic.ru

PowerShell *
powershell, active directory, windows, powershell2, .net, мониторинг, windows server, администрирование, microsoft, exchange

ASP *
asp.net mvc, asp.net, .net, c#, asp.net 5, asp.net mvc 3, javascript, конференция, asp.net mvc 4, mvc

MySQL *
mysql, php, sql, базы данных, mysql performance, innodb, myisam, percona, oracle, оптимизация


CTF *
Командные соревнования по инфобезу в формате CTF

Microsoft Edge
microsoft edge, edge, javascript, windows 10, html, shadow dom, web components, ortc, webrtc, hosted web apps

Чулан
стартапы
Forth *
Конкатенативный язык программирования

SAN *
netapp, netapp fas, san, схд, cdot, системы хранения данных, clustered ontap, linux, cisco ucs, fas

Smalltalk *
smalltalk, ооп, squeak, llst, little smalltalk, программирование, llvm, смолток, виртуальная машина, io

Julia *
julia

Laravel *
laravel, php, framework, laravel 5, laravel 4, composer, cms, vagrant, eloquent, open source

Xcode *
xcode, ios, ios development, objective-c, ios разработка, swift, разработка под ios, app store, iphone, ios sdk

IPv6 *
ipv6, ipv4, cisco, nat, hurricane electric, linux, интернет, dns, google, iana

Meteor.JS *
meteor, meteorjs, javascript, nodejs, node.js, meteor.js, coffeescript, framework, reactive programming, mongodb

Firebird/Interbase *
firebird, firebirdsql, sql, базы данных, open source, delphi, c++, c

SQLite *
sqlite, базы данных, sqlite3, ios, sql, c#, android, unicode, qt, .net

D *
d, dlang, программирование, c++, d2, dub, метапрограммирование, tutorials d, tutorials, язык d

Mesh-сети *
mesh, cjdns, hyperboria, mesh-сеть, mesh-сети, wi-fi, mesh сеть, социальные сети, openwrt, аниме

I2P *
i2p, c++, анонимность, tor, proxy, информационная безопасность, анонимность в сети, ed25519

Derby.js *
Full-stack framework
derbyjs, derby.js, web-разработка, node.js, fullstack development, реактивное программирование, туториал, nodejs, meteorjs, javascript

Разработка под Tizen *
tizen, samsung, mobile development, tizen sdk, мобильная разработка, html5, javascript, самсунг, intel, c++

Emacs *
emacs, linux, vim, ide, python, lisp, программирование, windows, org-mode, emacs-tips

Разработка под Bada *
bada, samsung, samsung bada, картография

Mercurial *
mercurial, git, hg, svn, fogcreek, системы контроля версий, commit, bitbucket.org, atlassian, version control

Lua *
lua, nginx, game development, gamedev, asterisk, c++, redis, робототехника, tarantool, ооп

UML Design *
uml, проектирование, mde, model-driven architecture, model-driven engineering, modelling, программирование, uml-проектирование, metamodel, mda

Fortran *
fortran, фортран

Dart *
dart, javascript, web, java, google, производительность

Cocoa *
objective-c, ios, cocoa, ios development, rambler, swift, di, ioc, dependency injection, typhoon

Cobol *
cobol, программирование, олдскул

Apache Flex *
flex, java, flash, intersystems cache, maven, объектные субд, cache, jade, подкасты, as3

Action Script *
flash, actionscript 3.0, actionscript, adobe air, as3, game development, flex, javascript, air, c#

Joomla *
joomla, cms, akeeba backup, информационная безопасность, шаблон

1С-Битрикс
битрикс, 1с-битрикс, php, bitrix, 1с, 1c интеграция, 1c, 1c 8.2, 1c-bitrix, битрикс24

CAD/CAM *
cad, сапр, nanocad, autocad, .net, api, multicad, c#, autodesk, cad online

Apache *
apache, apache2, nginx, python, java, htaccess, php, хостинг, mysql, apache spark

Вконтакте API *
вконтакте, вконтакте api, социальные сети, vkontakte api, vk api, vk.com, javascript, python, vk, api

Facebook API *
facebook, facebook api, open graph, fql, graph api, facebook timeline, unity3d, facebook apps, оптимизация, batch

Microsoft Access *
ms access, vba, офисное программирование, microsoft, visual basic for applications, sql, ms office

PDF
pdf, python, php, генерация pdf, хабрахабр, wpf, tutorial, java, wkhtmltopdf, xps

Разработка под Windows Phone *
windows phone, windows, c#, microsoft, nokia, windows 8, windows phone 8, разработка под windows phone, xaml, windows phone 7

Prolog *
prolog, пролог, haskell, логическое программирование, zebra puzzle, refal, logic, declarative, logic programming, программирование

GTK+ *
gtk, linux, gtk+, glade, gui, c, pygtk, gtkmm

Maps API *
maps, google maps, 2гис, карты, javascript, api 2гис, leaflet, maps api, 2гис онлайн, api карт

LabVIEW *
labview, bioloid, робототехника, bluetooth, плк, бесполезности, вариабельность сердечного ритма, алгоритмы

Cubrid *
cubrid, php

Canvas *
canvas, javascript, html5, libcanvas, atomjs, svg, dbcartajs, html5 canvas, webgl, fabricjs

OpenStreetMap *
openstreetmap, карты, osm, картография, россия, статистика, краудсорсинг, android, leaflet, google

Creative Commons *
creative commons, свободные лицензии, авторское право, копирайт, общественное достояние, гражданский кодекс, свободная лицензия, wikipedia, авторские права, open source

Mono и Moonlight *
mono, .net, monotouch, c#, xamarin, monodevelop, iphone, monodroid, android, c

Doctrine ORM *
php, doctrine, doctrine2, orm, mysql, symfony, symfony2, пагинация, sql_calc_found_rows, doctrine 2

Google App Engine *
google app engine, google, python, java, blobstore, datastore, appengine, taskqueue, статистика, gae from the beginning

Регулярные выражения *
регулярные выражения, regexp, regular expressions, regex, javascript, email, ruby, квантификаторы, программирование, ошибки

Fidonet *
fidonet, фидонет, фидо, fido, гипертекстовый фидонет, ftn, fidoip, golded, node.js на узле фидонета, nodejs

Twisted *
twisted, python, deferred, framework

IPTV *
iptv, телевидение, vlc, тв, wi-fi, hdtv, stb, билайн, udpxy, безопасность

Оболочки *
bash, shell, zsh, linux, оболочки, sh, script, частые ошибки, скрипты, fish

XSLT *
xslt, xml, php, xpath, cms, xhtml, xsl, dita open toolkit, шаблонизатор, дата

TDD *
tdd, unit testing, тестирование, bdd, юнит-тесты, test driven development, java, testing, javascript, unit-testing

LaTeX *
Набор макросов, надстройка над TeX
latex, pdf, tex, вёрстка, графика, lyx, python, tikz, картинки, формулы

Small Basic *
small basic, microsoft, новый релиз

Kohana *
kohana, kohana 3, php, framework, знакомство с kohana 3.0, mvc, кохана, kohana 3.2, orm, cms

Разработка под Java ME *
j2me, java, java me, development, javafx, sun, nokia, midlets, mobile, nokia asha

LiveStreet
livestreet, социальные сети, блогосфера, cms, движок, дизайн, livestreet cms, блогосоциальная сеть, движок социальной сети, modx revolution

MooTools *
mootools, javascript, ajax, анимация, jquery

Adobe Flash *
flash, as3, actionscript, adobe, flex, flash player, actionscript 3.0, iphone, adobe air, realaxy

GreaseMonkey *
greasemonkey, userscripts, firefox, хабрахабр, userscript, javascript, userjs, google chrome, safari, юзерскрипты

INFOLUST *
qr-code, qr, barcode, qr-код, qr code, штрих-коды, 2d-barcode, datamatrix, дизайн, мобильные метки

Groovy & Grails *
groovy, java, grails, dsl, functional programming, scala, jvm, clojure, gradle, ratpack

Lisp *
lisp, clojure, common lisp, scheme, функциональное программирование, пол грэм, dsl, программирование, zrf, zillions of games 2

Zend Framework *
zend framework, php, zend, zf, zend framework 2, zend_form, framework, mvc, zf2, zend_acl

Библиотека ExtJS/Sencha *
extjs, javascript, sencha, ajax, extjs 4, ext, framework, sencha touch, extjs4, ria

Internet Explorer
internet explorer, microsoft, internet explorer 9, ie9, ie8, браузеры, html5, javascript, internet explorer 8, ie11

jQuery *
jquery, javascript, jquery plugins, jquery ui, web-разработка, ajax, plugin, css, html5, jquery plugin

Silverlight
silverlight, microsoft, .net, silverlight 4, wpf, c#, silverlight 3, smooth streaming, expression, silverlight 2

SharePoint
sharepoint, sharepoint 2013, sharepoint 2010, microsoft, office 365, moss 2007, wss 3.0, azure, c#, powershell

Symfony *
symfony2, php, symfony, symfony 2, framework, symfony framework, doctrine, dependency injection, sonataadmin, micro-framework

Google Web Toolkit *
gwt, java, google web toolkit, google, javascript, web, framework, web-разработка, extjs, google app engine

Я пиарюсь
стартапы, социальные сети, web 2.0, конференция, инвестиции, android, сервисы, игры, идея, видео

Eclipse *
eclipse, java, ide, mda, mof, mde, model-driven engineering, ecore, metamodel, model-driven architecture

CakePHP *
cakephp, php, framework, релиз

MODX *
modx, modx revolution, modx evolution, интернет-магазин, cms, xpdo, shopkeeper, ditto, репозиторий, php

Глобальные системы позиционирования *
gps, глонасс, gps мониторинг, gps трекер, glonass, навигация, персональный мониторинг, android, мобильные приложения, прогород

Drupal *
drupal, drupal 7, php, cms, друпал, web-разработка, drupal-digest, drupal 8, views, дайджест

Работа с иконками *
иконки, icons, дизайн иконок, пиктограммы, дизайн, веб-дизайн, иконка, fugue, svg, бесплатно

Хабрахабр
хабрахабр, комментарии, идея, ошибки, карма, юзабилити, rss, баги, рейтинг, блогосфера

Opera
opera, opera software, opera mini, опера, браузеры, javascript, community, extensions, расширения, opera 10.50

Микроформаты *
микроформаты, schema.org, semantic web, микроразметка, hcard, яндекс.вебмастер, open graph, firefox, twitter, html5

WordPress *
wordpress, блогосфера, cms, plugins, php, плагины, безопасность, web-разработка, plugin, спам

Работа с векторной графикой *
svg, векторная графика, дизайн, inkscape, javascript, сапр, веб-дизайн, иконки, css, cad

CodeIgniter *
codeigniter, php, framework, mvc, web-разработка, codeigniter 2.1.0, mysql, imagecms, codeigniter hmvc, php 5.3

