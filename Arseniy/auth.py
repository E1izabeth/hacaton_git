from bson import ObjectId
from pymongo import MongoClient
import codecs

class Tag:
    def __init__(self, name='', avg_value=0.0, value_count=0, tag_dict=None):
        if tag_dict is None:
            self.name = name
            self.avg_value = avg_value
            self.value_count = value_count
        else:
            self.name = tag_dict['name']
            self.avg_value = tag_dict['avg_value']
            self.value_count = tag_dict['value_count']

    def to_dict(self):
        tag_dict = {'name': self.name, 'avg_value': self.avg_value, 'value_count': self.value_count}
        return tag_dict


class Model:
    def __init__(self, name='', short_descr='', long_descr='', model_dict=None):
        if model_dict is None:
            self.likes_count = 0
            self.likes_avg = 0.0
            self.list_tags = []
            self.short_descr = short_descr
            self.long_descr = long_descr
            self.diff_avg = 0.0
            self.diff_count = 0
            self.name = name
            self.relevant = 0
            self.id = -1
            self.type = 0
        else:
            self.name = model_dict['name']
            self.likes_count = int(model_dict['likes_count'])
            self.likes_avg = float(model_dict['likes_avg'])
            _list_tags = model_dict['list_tags']
            self.list_tags = []
            for tag in _list_tags:
                new_tag = Tag(tag_dict=tag)
                self.list_tags.append(new_tag)
            self.short_descr = model_dict['short_descr']
            self.long_descr = model_dict['long_descr']
            self.diff_avg = float(model_dict['diff_avg'])
            self.diff_count = int(model_dict['diff_count'])
            self.relevant = model_dict['relevant']
            self.id = model_dict['_id']
            self.type = int(model_dict['type'])

    def to_dict(self):
        model_dict = {'name': self.name, 'likes_count': self.likes_count, 'likes_avg': self.likes_avg,
                      'list_tags': self.list_tags, 'short_descr': self.short_descr, 'long_descr': self.long_descr,
                      'diff_avg': self.diff_avg, 'diff_count': self.diff_count,
                      'relevant': self.relevant, '_id': self.id, 'type': self.type}
        return model_dict


def get_models():
    client = MongoClient()
    db = client['SiteDB']
    models = db['Models']
    return models


def update_model(_id, diff_value, like_value):

    models = get_models()
    model_d = models.find({"_id": _id})
    model = Model(model_dict=model_d)
    if not((diff_value == 0) | (like_value == 0)):
        new_diff_value = model.diff_count * model.diff_avg + diff_value
        new_diff_count = model.diff_count + 1
        new_diff_avg = new_diff_value / new_diff_count
        new_likes_value = model.likes_count * model.likes_avg + like_value
        new_likes_count = model.likes_count + 1
        new_like_avg = new_likes_value / new_likes_count
    else:
        if diff_value == 0:
            new_diff_avg = model.diff_avg
            new_diff_count = model.diff_count
        if like_value == 0:
            new_like_avg = model.likes_avg
            new_likes_count = model.likes_count
    models.update_one(
        {"_id": _id},
        {"$set": {'diff_avg': new_diff_avg, 'diff_count': new_diff_count, 'like_avg': new_like_avg,
                  'likes_count': new_likes_count}}
    )


def set_new_model(model_dict=None, name=''):
    models = get_models()
    if model_dict is None:
        model_dict = {'name': name, 'likes_count': 1, 'likes_avg': 3,
                      'list_tags': [], 'short_descr': 'blabla', 'long_descr': 'ololo',
                      'diff_avg': 6, 'diff_count': 8, 'relevant': 0, 'type': 1}
    models.insert_one(model_dict)
    id = model_dict['_id']
    models.update_one(
        {"_id": id},
        {"$set": {"id": id}}
    )
    return id


def get_model_by_id(id):
    models = get_models()
    model_d = models.find_one({"_id": ObjectId(id)})
    # print(model_d['_id'])
    if model_d is None:
        print('No one model is gotten by this id')
        return None
    else:
        model = Model(model_dict=model_d)
        return model


def change_value(id, value_name, value):
    models_d = get_models()
    models_d.update_one(
        {"_id": ObjectId(id)},
        {"$set": {value_name: value}}
    )


def print_models():
    models = get_models()
    models_collection = models.find()
    for model_dict in models_collection:
        m = Model(model_dict=model_dict)
        print(m.name+'\n')
        for tag in m.list_tags:
            print(tag.name+' ')


def update_list_tags(list_tags, new_tag_d):
    new_tag = Tag(tag_dict=new_tag_d)
    is_matched = False
    for last_tag_d in list_tags:
        last_tag = Tag(tag_dict=last_tag_d)
        if last_tag.name == new_tag.name:
            is_matched = True
            ind = list_tags.index(last_tag_d)
            last_tag = add_new_rate(last_tag, new_tag.avg_value)
            list_tags[ind] = last_tag.to_dict()
    if not is_matched:
        list_tags.append(new_tag)
    return list_tags


def add_new_rate(tag_d, value):
    tag = Tag(tag_dict=tag_d)
    new_value = tag.value_count * tag.avg_value + value
    tag.value_count += 1
    tag.avg_value = new_value / tag.value_count
    return tag


def print_tag(tag_d):
    tag = Tag(tag_dict=tag_d)
    print(tag.name, tag.avg_value, tag.value_count)


def main():
    models = get_models()
    models.drop()
    f = codecs.open("filter.txt", "r", "utf_8_sig")
    models = get_models()
    for i in range(248):
        list_tags = []
        programm_name = f.readline()
        programm_name = programm_name[:-2]
        tag_names = f.readline().split(', ')
        for tag_name in tag_names:
            tag = Tag(tag_name)
            tag_d = tag.to_dict()
            list_tags.append(tag_d)
        id_mod = set_new_model(name=programm_name)
        models.update_one(
            {"_id": id_mod},
            {"$set": {"list_tags": list_tags}}
        )
        f.readline()
    print_models()
    f.close()


main()
